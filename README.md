# Metadata dictionary
This is a dhis app that enables the user to browse metadata of but not limited to datasets, organisation units, indicatiors and user. 
This can anable a user to search through metadata in a quick and easy way without going through alot of applicattions to get the metadata
details or learning how to use the api. the app can be easly extended to apply to all forms of metadata.

## Installation
Zip (compress) the app folder. Go to DHIS2 | App Management | upload the zipped file.

### Authors

1. Yamikani Phiri
2. Lawrence Byson
### Contributors